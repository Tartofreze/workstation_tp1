# Maîtrise de poste - Day 1

## Self-footprinting : 
### **Host OS :**
>**Nom de la Machine :**
```
systeminfo :
Affiche des informations de configuration détaillées sur un ordinateur et son système d’exploitation, notamment la configuration du système d’exploitation, les informations de sécurité, l’ID de produit et les propriétés matérielles)
```
>>Resultat : 
```
DESKTOP-DG9JKP6
```
***

>**OS et version :**
```
systeminfo :
Affiche des informations de configuration détaillées sur un ordinateur et son système d’exploitation, notamment la configuration du système d’exploitation, les informations de sécurité, l’ID de produit et les propriétés matérielles)
```
>>Resultat : 
```
OS : Microsoft Windows 10 Famille
Version : 10.0.19041 N/A version 19041
```
***

>**Architecture processeur :**

```
systeminfo :
Affiche des informations de configuration détaillées sur un ordinateur et son système d’exploitation, notamment la configuration du système d’exploitation, les informations de sécurité, l’ID de produit et les propriétés matérielles)
```
>>Resultat : 
```
x64-based PC
```
***

>**Quantité RAM et modèle de la RAM :**
```
systeminfo :
Affiche des informations de configuration détaillées sur un ordinateur et son système d’exploitation, notamment la configuration du système d’exploitation, les informations de sécurité, l’ID de produit et les propriétés matérielles)
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
wmic MemoryChip get BankLabel, Capacity, MemoryType, TypeDetail, Speed :
Vérification des détails complets de la RAM à l'aide de la commande WMIC

```
>>Resultat : 
```
BANK 0   |  8589934592 | ChannelA-DIMM0 | Physical Memory 0
BANK 2   |  8589934592 | ChannelB-DIMM0 | Physical Memory 2
Manufacturer : Micron
Speed=3200
```
***

## 

### **Devices :**

> **la marque et le modèle du processeur :**
```
wmic cpu get caption, deviceid, name, numberofcores, maxclockspeed, status :
Liste les details complet du ou des processeurs
-----------------------------------------------------------------------------------------
get-wmiobject win32_processor | select-object NumberOfCores, NumberOfLogicalProcessors :
Liste le nombre de coeurs et de processeurs logiques
```
>>Resultat : 
```
Name                                        
Intel(R) Core(TM) i7-10875H CPU @ 2.30GHz

NumberOfCores NumberOfLogicalProcessors
------------- -------------------------
            8                        16
```            
- Intel Core : Marque
- i7 : modificateur de marque
- **10**875H : Le 10 correspond a la 10ème génération de processeur 
- 10**875**H : 875 correspond aux Chiffres Sku
- 10875**H** : Le H correspond a la spécificité du processeur Hautes performances optimisé pour les ordinateurs portables

***

> **la marque et le modèle du touchpad\trackpad :**
```
driverquery /si :
liste tous les drivers d'installés et signés
```
>>Resultat : 
```
HID-compliant touch pad
ELAN Input Device
```  
- Frabriquant HID-compliant : Microsoft
- Fabriquant ELAN Input Device : ELAN

***

> **la marque et le modèle de la carte graphique :**
```
driverquery /si :
liste tous les drivers d'installés et signés
```
>>Resultat : 
```
NVIDIA GeForce RTX 2070 max-q design
``` 
- Fabriquant : Nvidia

***

**Disques Durs :**

>**Marque et modèle**
```
Get-Disk :
liste tous les disques durs
```
>>Resultat : 
```
Intel Optane+477GBSSD
TOSHIBA External USB 3.0
``` 

- Marque : Intel
- Marque Toshiba

>**Partitions**

```
Diskpart
sel disk <n> (n=numero du disque)
detail disk
list partition

Diskpart permet de manipuler les disques et partitions
sel disk permet de selectionner un disque
detail disk affiche les details du disque selectionné
list partition permet de lister toutes les partitions du disque selectionné
```
>>Resultat : 
```
  N° volume     Nom          Fs     Type            
  ----------    -----------  -----  ----------    
  Volume 0      Windows      NTFS   Partition           
  Volume 1      WinRE        NTFS   Partition     
  Volume 2      RecoveryIma  NTFS   Partition     
  Volume 3      SYSTEM       FAT32  Partition           

  N° volume     Nom          Fs     Type        
  ----------    -----------  -----  ----------  
  Volume 4      TOSHIBA EXT  NTFS   Partition     
``` 
- Fs = Systeme de fichier des partitions
- Fonctions : 
System : Partition du système EFI
Windows : Démarrer, Fichiers d’échanges, Vidage sur incident, Partition de données de base
WinRe : Partition de récupération
RecoveryIma : Partition de récupération
Toshiba EXT  : Actif, Partition Principale

## 

### **Users :**
>**liste des utilisateurs**
```
Get-WmiObject Win32_UserAccount
liste tous les utilisateurs de la machine
```
>>Resultat : 
```
AccountType : 512
Caption     : DESKTOP-DG9JKP6\Administrateur
Domain      : DESKTOP-DG9JKP6
SID         : S-1-5-21-374798112-2101922778-2622320536-500
FullName    :
Name        : Administrateur

AccountType : 512
Caption     : DESKTOP-DG9JKP6\DefaultAccount
Domain      : DESKTOP-DG9JKP6
SID         : S-1-5-21-374798112-2101922778-2622320536-503
FullName    :
Name        : DefaultAccount

AccountType : 512
Caption     : DESKTOP-DG9JKP6\Invité
Domain      : DESKTOP-DG9JKP6
SID         : S-1-5-21-374798112-2101922778-2622320536-501
FullName    :
Name        : Invité

AccountType : 512
Caption     : DESKTOP-DG9JKP6\mattf
Domain      : DESKTOP-DG9JKP6
SID         : S-1-5-21-374798112-2101922778-2622320536-1001
FullName    : matthias flament
Name        : mattf

AccountType : 512
Caption     : DESKTOP-DG9JKP6\virtu
Domain      : DESKTOP-DG9JKP6
SID         : S-1-5-21-374798112-2101922778-2622320536-1003
FullName    :
Name        : virtu

AccountType : 512
Caption     : DESKTOP-DG9JKP6\WDAGUtilityAccount
Domain      : DESKTOP-DG9JKP6
SID         : S-1-5-21-374798112-2101922778-2622320536-504
FullName    :
Name        : WDAGUtilityAccount    
``` 
>**nom de l'utilisateur full admin**
```
?
```

## 

### **Processus :**
>**liste des processus**
```
tasklist
Affiche la liste des processus actuellement en cours d’exécution sur l’ordinateur local ou sur un ordinateur distant
```
>>Resultat : 
```
System Idle Process              0 Services                   0         8 Ko
System                           4 Services                   0    10 568 Ko
Registry                       172 Services                   0    56 224 Ko
smss.exe                       748 Services                   0       400 Ko
csrss.exe                      832 Services                   0     2 696 Ko
wininit.exe                    572 Services                   0     3 152 Ko
csrss.exe                      768 Console                    1     3 780 Ko
services.exe                   876 Services                   0     7 476 Ko
lsass.exe                      884 Services                   0    18 404 Ko
svchost.exe                    992 Services                   0     1 300 Ko
svchost.exe                   1048 Services                   0    26 012 Ko
fontdrvhost.exe               1056 Services                   0     1 476 Ko
winlogon.exe                  1156 Console                    1     7 724 Ko
fontdrvhost.exe               1200 Console                    1     7 400 Ko
svchost.exe                   1208 Services                   0    13 900 Ko
svchost.exe                   1300 Services                   0     6 156 Ko
WUDFHost.exe                  1328 Services                   0     3 736 Ko
dwm.exe                       1420 Console                    1   255 956 Ko
svchost.exe                   1536 Services                   0     6 444 Ko
svchost.exe                   1572 Services                   0     4 392 Ko
svchost.exe                   1580 Services                   0     8 264 Ko
svchost.exe                   1588 Services                   0     8 528 Ko
svchost.exe                   1724 Services                   0     6 820 Ko
svchost.exe                   1732 Services                   0     8 028 Ko
svchost.exe                   1872 Services                   0    11 228 Ko
svchost.exe                   1928 Services                   0    10 756 Ko
svchost.exe                   1936 Services                   0     2 696 Ko
svchost.exe                   1976 Services                   0     4 460 Ko
svchost.exe                   2000 Services                   0     8 388 Ko
svchost.exe                   2008 Services                   0     2 796 Ko
svchost.exe                   2172 Services                   0     4 620 Ko
svchost.exe                   2232 Services                   0     6 992 Ko
svchost.exe                   2268 Services                   0     5 376 Ko
svchost.exe                   2324 Services                   0     6 156 Ko
svchost.exe                   2368 Services                   0     4 940 Ko
svchost.exe                   2428 Services                   0     5 248 Ko
NVDisplay.Container.exe       2504 Services                   0    12 236 Ko
svchost.exe                   2588 Services                   0     8 676 Ko
svchost.exe                   2620 Services                   0     3 056 Ko
svchost.exe                   2648 Services                   0     4 412 Ko
svchost.exe                   2660 Services                   0     8 228 Ko
svchost.exe                   2760 Services                   0     3 216 Ko
svchost.exe                   2812 Services                   0     2 396 Ko
svchost.exe                   2820 Services                   0    10 152 Ko
svchost.exe                   2836 Services                   0     4 184 Ko
svchost.exe                   2900 Services                   0     5 040 Ko
svchost.exe                   2944 Services                   0     6 748 Ko
svchost.exe                   3040 Services                   0    14 588 Ko
Memory Compression            3060 Services                   0   438 724 Ko
svchost.exe                   2488 Services                   0     4 004 Ko
igfxCUIService.exe            3136 Services                   0     4 128 Ko
svchost.exe                   3188 Services                   0     4 004 Ko
svchost.exe                   3196 Services                   0     3 944 Ko
NVDisplay.Container.exe       3356 Console                    1    39 996 Ko
svchost.exe                   3448 Services                   0    11 572 Ko
audiodg.exe                   3572 Services                   0    19 592 Ko
svchost.exe                   3680 Services                   0     5 560 Ko
svchost.exe                   3688 Services                   0     3 624 Ko
svchost.exe                   3696 Services                   0     6 720 Ko
svchost.exe                   3844 Services                   0     4 416 Ko
svchost.exe                   3920 Services                   0    13 464 Ko
svchost.exe                   4076 Services                   0    19 336 Ko
svchost.exe                   3480 Services                   0     8 772 Ko
spoolsv.exe                   3108 Services                   0     9 388 Ko
svchost.exe                   4172 Services                   0     6 968 Ko
svchost.exe                   4228 Services                   0     9 412 Ko
wlanext.exe                   4280 Services                   0     1 880 Ko
conhost.exe                   4292 Services                   0     1 604 Ko
svchost.exe                   4544 Services                   0     6 644 Ko
svchost.exe                   4552 Services                   0    14 548 Ko
svchost.exe                   4560 Services                   0     8 540 Ko
ELANFPService.exe             4568 Services                   0     1 332 Ko
svchost.exe                   4580 Services                   0    22 708 Ko
svchost.exe                   4588 Services                   0    45 264 Ko
svchost.exe                   4608 Services                   0     1 636 Ko
SMV4_Service.exe              4624 Services                   0     5 500 Ko
RtkAudUService64.exe          4632 Services                   0     4 276 Ko
NahimicService.exe            4640 Services                   0     5 900 Ko
svchost.exe                   4648 Services                   0     3 820 Ko
svchost.exe                   4656 Services                   0    16 348 Ko
IntelCpHDCPSvc.exe            4664 Services                   0     3 240 Ko
svchost.exe                   4680 Services                   0     2 376 Ko
ThunderboltService.exe        4688 Services                   0     5 244 Ko
pservice.exe                  4696 Services                   0     2 880 Ko
nvcontainer.exe               4712 Services                   0    21 604 Ko
KillerAnalyticsService.ex     4716 Services                   0     5 820 Ko
RstMwService.exe              4728 Services                   0     1 968 Ko
lghub_updater.exe             4748 Services                   0     3 808 Ko
MsMpEng.exe                   4808 Services                   0   194 392 Ko
OfficeClickToRun.exe          4828 Services                   0    21 048 Ko
svchost.exe                   5040 Services                   0     2 856 Ko
IntelCpHeciSvc.exe            5204 Services                   0     2 788 Ko
jhi_service.exe               5272 Services                   0     1 520 Ko
svchost.exe                   5284 Services                   0     1 848 Ko
KillerNetworkService.exe      5360 Services                   0    35 804 Ko
xTendUtilityService.exe       5516 Services                   0     4 984 Ko
KSPSService.exe               5524 Services                   0     5 000 Ko
svchost.exe                   5644 Services                   0     6 268 Ko
svchost.exe                   5948 Services                   0     7 172 Ko
XtuService.exe                6260 Services                   0    18 508 Ko
svchost.exe                   6328 Services                   0    13 516 Ko
KAPSService.exe               6356 Services                   0     4 804 Ko
KSPS.exe                      6552 Services                   0     1 856 Ko
xTendUtility.exe              6568 Services                   0     2 676 Ko
KAPS.exe                      6576 Services                   0     7 732 Ko
conhost.exe                   6588 Services                   0     3 960 Ko
conhost.exe                   6596 Services                   0     3 976 Ko
conhost.exe                   6612 Services                   0     4 232 Ko
rundll32.exe                  6524 Console                    1     3 048 Ko
WmiPrvSE.exe                  7664 Services                   0    32 292 Ko
WmiPrvSE.exe                  7884 Services                   0     7 304 Ko
NisSrv.exe                    6948 Services                   0     8 100 Ko
svchost.exe                   6240 Services                   0     1 732 Ko
nvcontainer.exe               8300 Console                    1    27 256 Ko
nvcontainer.exe               8324 Console                    1    36 252 Ko
sihost.exe                    8360 Console                    1    23 556 Ko
svchost.exe                   8384 Console                    1    20 140 Ko
svchost.exe                   8396 Console                    1     4 124 Ko
PresentationFontCache.exe     8436 Services                   0     5 100 Ko
svchost.exe                   8484 Console                    1    28 952 Ko
taskhostw.exe                 8576 Console                    1    16 176 Ko
NVIDIA RTX Voice.exe          8588 Console                    1    35 252 Ko
svchost.exe                   8608 Services                   0    12 516 Ko
svchost.exe                   8740 Services                   0     3 780 Ko
ctfmon.exe                    8820 Console                    1    18 940 Ko
explorer.exe                  9020 Console                    1   175 432 Ko
svchost.exe                   8180 Services                   0    13 404 Ko
svchost.exe                   9356 Services                   0     4 372 Ko
igfxEM.exe                    9364 Console                    1    11 472 Ko
svchost.exe                   9564 Services                   0     7 912 Ko
svchost.exe                   9668 Services                   0     4 456 Ko
svchost.exe                   9712 Services                   0     5 560 Ko
svchost.exe                  10060 Console                    1    19 664 Ko
SearchIndexer.exe            10176 Services                   0    52 392 Ko
dtyWork.exe                  10292 Console                    1     7 480 Ko
StartMenuExperienceHost.e    10640 Console                    1    58 528 Ko
RuntimeBroker.exe            11040 Console                    1    20 108 Ko
SearchApp.exe                 1508 Console                    1   301 624 Ko
GMSG.exe                       912 Console                    1    10 600 Ko
svchost.exe                   2572 Services                   0     8 380 Ko
RuntimeBroker.exe             9176 Console                    1    42 680 Ko
FusionShortcut.exe            2776 Console                    1     8 368 Ko
svchost.exe                  11480 Services                   0    14 560 Ko
YourPhone.exe                11608 Console                    1       572 Ko
SettingSyncHost.exe          11740 Console                    1     6 852 Ko
LockApp.exe                  12092 Console                    1    31 420 Ko
RuntimeBroker.exe            12152 Console                    1    23 836 Ko
RuntimeBroker.exe            12416 Console                    1    23 852 Ko
GoogleCrashHandler.exe       13052 Services                   0     1 440 Ko
GoogleCrashHandler64.exe     13084 Services                   0       340 Ko
nvsphelper64.exe              9640 Console                    1     5 740 Ko
NVIDIA Share.exe             12584 Console                    1    49 760 Ko
dllhost.exe                  11076 Services                   0     5 760 Ko
NVIDIA Share.exe             12976 Console                    1    28 256 Ko
NVIDIA Web Helper.exe        13496 Console                    1    39 624 Ko
conhost.exe                  13504 Console                    1     2 288 Ko
svchost.exe                  13700 Services                   0    11 492 Ko
RuntimeBroker.exe            14024 Console                    1     9 424 Ko
SecurityHealthSystray.exe     9448 Console                    1     4 996 Ko
SecurityHealthService.exe    12964 Services                   0     7 960 Ko
RtkAudUService64.exe          9980 Console                    1     3 608 Ko
NVIDIA Share.exe             12116 Console                    1    65 820 Ko
FusionStation.exe            14328 Console                    1    52 172 Ko
Discord.exe                   9200 Console                    1    65 588 Ko
lghub.exe                    14108 Console                    1    43 672 Ko
steam.exe                    13808 Console                    1    52 312 Ko
Discord.exe                   8728 Console                    1   125 156 Ko
lghub_agent.exe              14112 Console                    1    32 456 Ko
Discord.exe                  13332 Console                    1    35 320 Ko
lghub.exe                    10104 Console                    1    40 460 Ko
lghub.exe                    12344 Console                    1    27 564 Ko
steamwebhelper.exe           15244 Console                    1    41 028 Ko
SteamService.exe             15352 Services                   0     3 740 Ko
Discord.exe                  14348 Console                    1    45 836 Ko
unsecapp.exe                 14432 Console                    1     5 124 Ko
steamwebhelper.exe           14512 Console                    1     8 816 Ko
EpicGamesLauncher.exe        14496 Console                    1    74 424 Ko
Discord.exe                  14668 Console                    1   286 272 Ko
steamwebhelper.exe           14776 Console                    1    37 780 Ko
steamwebhelper.exe           15108 Console                    1    22 240 Ko
CloudMatrixBattery.exe       15096 Console                    1    54 832 Ko
parsecd.exe                  15828 Console                    1    29 428 Ko
Cortana.exe                  15988 Console                    1    48 868 Ko
XboxAppServices.exe          16280 Console                    1    14 524 Ko
RuntimeBroker.exe            15516 Console                    1    18 340 Ko
svchost.exe                  15532 Services                   0    11 032 Ko
svchost.exe                  15544 Services                   0    25 600 Ko
Discord.exe                  16620 Console                    1    51 848 Ko
SpeechRuntime.exe            16980 Console                    1    12 956 Ko
jusched.exe                  17084 Console                    1     1 784 Ko
OSDwindow.exe                17116 Console                    1    26 840 Ko
svchost.exe                  16876 Console                    1    11 940 Ko
UnrealCEFSubProcess.exe      17272 Console                    1    23 556 Ko
ControlCenter.exe            17876 Console                    1   172 260 Ko
WUDFHost.exe                 18196 Services                   0     3 732 Ko
unsecapp.exe                 16548 Console                    1     3 984 Ko
ShellExperienceHost.exe      17680 Console                    1    30 428 Ko
RuntimeBroker.exe            17484 Console                    1    13 028 Ko
steamwebhelper.exe           16812 Console                    1    31 716 Ko
steamwebhelper.exe           16936 Console                    1    79 932 Ko
steamwebhelper.exe            3148 Console                    1    59 536 Ko
ApplicationFrameHost.exe     18448 Console                    1    30 156 Ko
WinStore.App.exe             18472 Console                    1       732 Ko
RuntimeBroker.exe            18660 Console                    1     9 304 Ko
Video.UI.exe                  2448 Console                    1       604 Ko
RuntimeBroker.exe            18192 Console                    1     4 260 Ko
Calculator.exe                4740 Console                    1       632 Ko
TextInputHost.exe             5580 Console                    1    33 584 Ko
svchost.exe                  19128 Services                   0     3 360 Ko
svchost.exe                  12576 Console                    1     6 644 Ko
svchost.exe                   2600 Services                   0     5 128 Ko
svchost.exe                   9440 Services                   0    12 848 Ko
svchost.exe                   3004 Services                   0     8 508 Ko
SgrmBroker.exe               19400 Services                   0     5 548 Ko
svchost.exe                  16696 Services                   0    14 824 Ko
MoUsoCoreWorker.exe          16608 Services                   0    13 940 Ko
svchost.exe                  17620 Services                   0     6 024 Ko
dllhost.exe                  18560 Console                    1    13 276 Ko
svchost.exe                  18900 Services                   0    15 180 Ko
Microsoft.Photos.exe         13296 Console                    1     1 100 Ko
svchost.exe                   3548 Services                   0    10 020 Ko
svchost.exe                  10924 Services                   0     6 112 Ko
RuntimeBroker.exe             8628 Console                    1    12 232 Ko
CompPkgSrv.exe                3368 Console                    1     8 220 Ko
vmnat.exe                    11816 Services                   0     6 628 Ko
vmnetdhcp.exe                12884 Services                   0     5 524 Ko
vmware-usbarbitrator64.ex     7048 Services                   0    11 036 Ko
vmware-authd.exe              3276 Services                   0    12 680 Ko
svchost.exe                   5584 Services                   0     5 664 Ko
launcher.exe                  5192 Console                    1    62 592 Ko
QtWebEngineProcess.exe       14680 Console                    1    39 076 Ko
chrome.exe                    9692 Console                    1   200 428 Ko
chrome.exe                    7728 Console                    1     7 112 Ko
chrome.exe                   18312 Console                    1   197 444 Ko
chrome.exe                   13328 Console                    1    41 404 Ko
chrome.exe                     632 Console                    1   113 972 Ko
powershell.exe                9580 Console                    1    77 616 Ko
conhost.exe                  13604 Console                    1    16 776 Ko
chrome.exe                    4200 Console                    1    95 628 Ko
chrome.exe                    5212 Console                    1   148 288 Ko
chrome.exe                   13616 Console                    1    48 988 Ko
notepad.exe                  10164 Console                    1    17 444 Ko
notepad.exe                      8 Console                    1    17 976 Ko
chrome.exe                    6716 Console                    1    70 640 Ko
svchost.exe                   2460 Services                   0     9 532 Ko
chrome.exe                   11228 Console                    1    77 880 Ko
chrome.exe                   13540 Console                    1    21 900 Ko
smartscreen.exe              11504 Console                    1    22 728 Ko
SearchProtocolHost.exe         592 Console                    1     8 276 Ko
SearchFilterHost.exe         12800 Services                   0     7 328 Ko
tasklist.exe                  6852 Console                    1     8 936 Ko
```
>**liste des services système**
```
 explorer.exe
 Csrss.exe
 Lsass.exe
 Services.exe
 Svchost.exe
```
- explorer.exe : Il s'agit de l'interface utilisateur, celle qui nous présente le bureau de Windows, la barre des tâches etc
- Csrss.exe : Il s'agit de la portion dite de mode utilisateur du sous-système Win32
- Lsass.exe : l s'agit du serveur local d'authentification de sécurité, il génère le processus responsable de l'authentification des utilisateurs par le service Winlogon
- Services.exe : Il s'agit du Service Control Manager (gestionnaire de contrôle des services) qui est responsable du démarrage, de l'arrêt et de l'interaction avec les services système
- Svchost.exe : Il s'agit d'un processus générique, il fonctionne en tant qu'hôte pour d'autres processus tournant à partir de Dlls, il peut y avoir plusieurs entrées pour ce processus

>**liste des processus lancés par le full admin**
```
?
```
>**Résultat**
```
?
```
## 

### **Network :**
>**liste des cartes réseau**
```
systeminfo
Affiche les informations du système
```
>>Resultat : 
```
[01]: Killer(R) Wi-Fi 6 AX1650x 160MHz Wireless Network Adapter (200NGW)
                                                  Nom de la connexion : Wi-Fi
                                                  DHCP activé :         Oui
                                                  Serveur DHCP :        10.33.19.254
                                                  Adresse(s) IP
                                                  [01]: 10.33.19.63
                                            [02]: Killer E2600 Gigabit Ethernet Controller
                                                  Nom de la connexion : Ethernet
                                                  État :                Support déconnecté
                                            [03]: Bluetooth Device (Personal Area Network)
                                                  Nom de la connexion : Connexion réseau Bluetooth
                                                  État :                Support déconnecté
                                            [04]: TAP-Windows Adapter V9
                                                  Nom de la connexion : Connexion au réseau local 2
                                                  État :                Support déconnecté
```
- [01]: Killer(R) Wi-Fi 6 AX1650x 160MHz Wireless Network Adapter (200NGW) : Carte Wifi
- [02]: Killer E2600 Gigabit Ethernet Controller : Carte Ethernet
- [03]: Bluetooth Device (Personal Area Network) : Carte Bluetooth
- [04]: TAP-Windows Adapter V9 : Pilote Réseau

>**liste des ports TCP et UDP en utilisation**
```
Netsat -a
liste tous les ports ouverts
```
>**Résultat**
```
  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:135            DESKTOP-DG9JKP6:0      LISTENING
  TCP    0.0.0.0:445            DESKTOP-DG9JKP6:0      LISTENING
  TCP    0.0.0.0:5040           DESKTOP-DG9JKP6:0      LISTENING
  TCP    0.0.0.0:7680           DESKTOP-DG9JKP6:0      LISTENING
  TCP    0.0.0.0:27036          DESKTOP-DG9JKP6:0      LISTENING
  TCP    0.0.0.0:49664          DESKTOP-DG9JKP6:0      LISTENING
  TCP    0.0.0.0:49665          DESKTOP-DG9JKP6:0      LISTENING
  TCP    0.0.0.0:49666          DESKTOP-DG9JKP6:0      LISTENING
  TCP    0.0.0.0:49667          DESKTOP-DG9JKP6:0      LISTENING
  TCP    0.0.0.0:49668          DESKTOP-DG9JKP6:0      LISTENING
  TCP    0.0.0.0:49669          DESKTOP-DG9JKP6:0      LISTENING
  TCP    0.0.0.0:49671          DESKTOP-DG9JKP6:0      LISTENING
  TCP    127.0.0.1:5309         DESKTOP-DG9JKP6:0      LISTENING
  TCP    127.0.0.1:6463         DESKTOP-DG9JKP6:0      LISTENING
  TCP    127.0.0.1:6463         DESKTOP-DG9JKP6:50051  ESTABLISHED
  TCP    127.0.0.1:9010         DESKTOP-DG9JKP6:0      LISTENING
  TCP    127.0.0.1:9010         DESKTOP-DG9JKP6:49746  ESTABLISHED
  TCP    127.0.0.1:9080         DESKTOP-DG9JKP6:0      LISTENING
  TCP    127.0.0.1:9100         DESKTOP-DG9JKP6:0      LISTENING
  TCP    127.0.0.1:9100         DESKTOP-DG9JKP6:49749  ESTABLISHED
  TCP    127.0.0.1:9180         DESKTOP-DG9JKP6:0      LISTENING
  TCP    127.0.0.1:27060        DESKTOP-DG9JKP6:0      LISTENING
  TCP    127.0.0.1:45654        DESKTOP-DG9JKP6:0      LISTENING
  TCP    127.0.0.1:49717        DESKTOP-DG9JKP6:65001  ESTABLISHED
  TCP    127.0.0.1:49718        DESKTOP-DG9JKP6:0      LISTENING
  TCP    127.0.0.1:49718        DESKTOP-DG9JKP6:49736  ESTABLISHED
  TCP    127.0.0.1:49736        DESKTOP-DG9JKP6:49718  ESTABLISHED
  TCP    127.0.0.1:49746        DESKTOP-DG9JKP6:9010   ESTABLISHED
  TCP    127.0.0.1:49749        DESKTOP-DG9JKP6:9100   ESTABLISHED
  TCP    127.0.0.1:50051        DESKTOP-DG9JKP6:6463   ESTABLISHED
  TCP    127.0.0.1:65001        DESKTOP-DG9JKP6:0      LISTENING
  TCP    127.0.0.1:65001        DESKTOP-DG9JKP6:49717  ESTABLISHED
  TCP    192.168.0.31:139       DESKTOP-DG9JKP6:0      LISTENING
  TCP    192.168.0.31:49748     162.159.136.234:https  ESTABLISHED
  ```

>**liste des ports TCP et UDP en utilisation**
```
netstat -a -b -o
Affiche toutes les connexions et ports utilisés, les exécutables qui sont liés à l'utilisation des ports et des connexions, l'identifiant du processus qui utilise le port
```
>>**Résultat**
```
Proto  Adresse locale         Adresse distante       tat
  TCP    0.0.0.0:135            DESKTOP-DG9JKP6:0      LISTENING       1232
  RpcEptMapper
 [svchost.exe]
  TCP    0.0.0.0:445            DESKTOP-DG9JKP6:0      LISTENING       4
 Impossible d'obtenir les informations de propri‚taire
  TCP    0.0.0.0:5040           DESKTOP-DG9JKP6:0      LISTENING       9504
  CDPSvc
 [svchost.exe]
  TCP    0.0.0.0:7680           DESKTOP-DG9JKP6:0      LISTENING       14856
 Impossible d'obtenir les informations de propri‚taire
  TCP    0.0.0.0:27036          DESKTOP-DG9JKP6:0      LISTENING       13680
 [steam.exe]
  TCP    0.0.0.0:49664          DESKTOP-DG9JKP6:0      LISTENING       756
 [lsass.exe]
  TCP    0.0.0.0:49665          DESKTOP-DG9JKP6:0      LISTENING       924
 Impossible d'obtenir les informations de propri‚taire
  TCP    0.0.0.0:49666          DESKTOP-DG9JKP6:0      LISTENING       1736
  EventLog
 [svchost.exe]
  TCP    0.0.0.0:49667          DESKTOP-DG9JKP6:0      LISTENING       2396
  Schedule
 [svchost.exe]
  TCP    0.0.0.0:49668          DESKTOP-DG9JKP6:0      LISTENING       2852
  SessionEnv
 [svchost.exe]
  TCP    0.0.0.0:49669          DESKTOP-DG9JKP6:0      LISTENING       3456
 [spoolsv.exe]
  TCP    0.0.0.0:49671          DESKTOP-DG9JKP6:0      LISTENING       1012
 Impossible d'obtenir les informations de propri‚taire
  TCP    127.0.0.1:5309         DESKTOP-DG9JKP6:0      LISTENING       15256
 [parsecd.exe]
  TCP    127.0.0.1:6463         DESKTOP-DG9JKP6:0      LISTENING       13580
 [Discord.exe]
  TCP    127.0.0.1:6463         DESKTOP-DG9JKP6:50051  ESTABLISHED     13580
 [Discord.exe]
  TCP    127.0.0.1:9010         DESKTOP-DG9JKP6:0      LISTENING       11280
 [lghub_agent.exe]
  TCP    127.0.0.1:9010         DESKTOP-DG9JKP6:49746  ESTABLISHED     11280
 [lghub_agent.exe]
  TCP    127.0.0.1:9080         DESKTOP-DG9JKP6:0      LISTENING       11280
 [lghub_agent.exe]
  TCP    127.0.0.1:9100         DESKTOP-DG9JKP6:0      LISTENING       4764
 [lghub_updater.exe]
  TCP    127.0.0.1:9100         DESKTOP-DG9JKP6:49749  ESTABLISHED     4764
 [lghub_updater.exe]
  TCP    127.0.0.1:9180         DESKTOP-DG9JKP6:0      LISTENING       4764
 [lghub_updater.exe]
  TCP    127.0.0.1:27060        DESKTOP-DG9JKP6:0      LISTENING       13680
 [steam.exe]
  TCP    127.0.0.1:45654        DESKTOP-DG9JKP6:0      LISTENING       11280
 [lghub_agent.exe]
  TCP    127.0.0.1:49717        DESKTOP-DG9JKP6:65001  ESTABLISHED     4700
 [nvcontainer.exe]
  TCP    127.0.0.1:49718        DESKTOP-DG9JKP6:0      LISTENING       9192
 [NVIDIA Web Helper.exe]
  TCP    127.0.0.1:49718        DESKTOP-DG9JKP6:49736  ESTABLISHED     9192
 [NVIDIA Web Helper.exe]
  TCP    127.0.0.1:49718        DESKTOP-DG9JKP6:51438  FIN_WAIT_2      9192
 [NVIDIA Web Helper.exe]
  TCP    127.0.0.1:49718        DESKTOP-DG9JKP6:51439  ESTABLISHED     9192
 [NVIDIA Web Helper.exe]
  TCP    127.0.0.1:49718        DESKTOP-DG9JKP6:51440  FIN_WAIT_2      9192
 [NVIDIA Web Helper.exe]
  TCP    127.0.0.1:49736        DESKTOP-DG9JKP6:49718  ESTABLISHED     7580
 [NVIDIA Share.exe]
  TCP    127.0.0.1:49746        DESKTOP-DG9JKP6:9010   ESTABLISHED     13040
 [lghub.exe]
  TCP    127.0.0.1:49749        DESKTOP-DG9JKP6:9100   ESTABLISHED     11280
 [lghub_agent.exe]
  TCP    127.0.0.1:50051        DESKTOP-DG9JKP6:6463   ESTABLISHED     11280
 [lghub_agent.exe]
  TCP    127.0.0.1:51438        DESKTOP-DG9JKP6:49718  CLOSE_WAIT      7580
 [NVIDIA Share.exe]
  TCP    127.0.0.1:51439        DESKTOP-DG9JKP6:49718  ESTABLISHED     7580
 [NVIDIA Share.exe]
  TCP    127.0.0.1:51440        DESKTOP-DG9JKP6:49718  CLOSE_WAIT      7580
 [NVIDIA Share.exe]
  TCP    127.0.0.1:65001        DESKTOP-DG9JKP6:0      LISTENING       4700
 [nvcontainer.exe]
  TCP    127.0.0.1:65001        DESKTOP-DG9JKP6:49717  ESTABLISHED     4700
 [nvcontainer.exe]
  TCP    192.168.0.31:139       DESKTOP-DG9JKP6:0      LISTENING       4
 Impossible d'obtenir les informations de propri‚taire
  TCP    192.168.0.31:49748     162.159.136.234:https  ESTABLISHED     12780
 [Discord.exe]
  TCP    192.168.0.31:49939     ec2-35-171-235-150:https  ESTABLISHED     15256
 [parsecd.exe]
  TCP    192.168.0.31:49962     47:https               ESTABLISHED     12780
 [Discord.exe]
  TCP    192.168.0.31:49966     ec2-52-201-104-53:https  ESTABLISHED     14296
 [EpicGamesLauncher.exe]
  TCP    192.168.0.31:50552     162.159.137.234:https  ESTABLISHED     12780
 [Discord.exe]
  TCP    192.168.0.31:50568     40.67.254.36:https     ESTABLISHED     4576
  WpnService
 [svchost.exe]
  TCP    192.168.0.31:50579     wn-in-f188:https       ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:50592     ec2-18-179-225-220:https  ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51025     151.101.122.132:https  ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51193     162.159.133.233:https  ESTABLISHED     12780
 [Discord.exe]
  TCP    192.168.0.31:51214     server-13-225-38-35:http  ESTABLISHED     14296
 [EpicGamesLauncher.exe]
  TCP    192.168.0.31:51215     server-13-225-38-35:http  ESTABLISHED     14296
 [EpicGamesLauncher.exe]
  TCP    192.168.0.31:51216     server-13-225-38-35:http  ESTABLISHED     14296
 [EpicGamesLauncher.exe]
  TCP    192.168.0.31:51221     a104-80-20-101:https   ESTABLISHED     10332
 [Video.UI.exe]
  TCP    192.168.0.31:51228     104.43.15.0:ms-sql-s   TIME_WAIT       0
  TCP    192.168.0.31:51254     server-13-225-38-35:http  ESTABLISHED     14296
 [EpicGamesLauncher.exe]
  TCP    192.168.0.31:51255     ec2-52-54-178-155:https  ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51257     ec2-52-54-178-155:https  ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51258     server-13-225-38-35:http  ESTABLISHED     14296
 [EpicGamesLauncher.exe]
  TCP    192.168.0.31:51261     server-13-225-38-35:http  ESTABLISHED     14296
 [EpicGamesLauncher.exe]
  TCP    192.168.0.31:51262     server-13-225-38-35:http  ESTABLISHED     14296
 [EpicGamesLauncher.exe]
  TCP    192.168.0.31:51263     server-13-225-38-35:http  ESTABLISHED     14296
 [EpicGamesLauncher.exe]
  TCP    192.168.0.31:51264     server-13-225-38-35:http  ESTABLISHED     14296
 [EpicGamesLauncher.exe]
  TCP    192.168.0.31:51266     server-13-225-38-35:http  ESTABLISHED     14296
 [EpicGamesLauncher.exe]
  TCP    192.168.0.31:51267     server-13-225-38-35:http  ESTABLISHED     14296
 [EpicGamesLauncher.exe]
  TCP    192.168.0.31:51270     par10s28-in-f110:https  CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51271     par10s28-in-f110:https  CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51272     server-13-225-38-35:http  ESTABLISHED     14296
 [EpicGamesLauncher.exe]
  TCP    192.168.0.31:51273     162.159.133.232:https  ESTABLISHED     12780
 [Discord.exe]
  TCP    192.168.0.31:51275     162.159.133.232:https  ESTABLISHED     12780
 [Discord.exe]
  TCP    192.168.0.31:51280     167:https              ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51281     20.44.232.74:https     ESTABLISHED     8700
  CDPUserSvc_b8a55
 [svchost.exe]
  TCP    192.168.0.31:51283     a23-62-197-154:https   ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51284     a23-62-197-154:https   ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51285     a23-62-197-154:https   ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51288     104.19.183.2:https     ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51289     server-13-225-25-71:https  ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51290     104.19.183.2:https     ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51291     server-13-225-25-71:https  ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51292     a23-62-197-154:https   ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51294     ec2-52-51-146-233:https  ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51297     49:https               ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51298     49:https               ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51299     49:https               ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51300     49:https               ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51301     ec2-54-77-78-90:https  ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51302     ec2-15-237-22-192:https  ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51303     ad9411418cf2cdacd:https  ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51306     server-143-204-217-93:https  ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51307     a23-206-5-209:https    ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51309     151.101.122.133:https  ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51313     104.19.184.2:https     ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51314     104.19.184.2:https     ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51322     148:https              ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51324     ec2-52-49-47-228:https  CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51328     148:https              ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51330     732:https              CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51331     ec2-52-49-47-228:https  CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51332     ec2-52-29-76-41:https  CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51334     a23-57-80-56:https     LAST_ACK        12924
 [chrome.exe]
  TCP    192.168.0.31:51345     185.29.133.58:https    CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51346     185.29.133.58:https    ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51350     dsp:https              CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51353     ec2-52-18-251-47:https  ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51355     ec2-52-18-251-47:https  CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51359     ip-185-184-8-30:https  ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51360     ec2-52-57-150-20:https  CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51361     136.144.49.28:https    CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51362     69.173.151.90:https    ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51363     46:https               ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51366     69.173.151.90:https    ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51367     46:https               ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51370     ec2-52-49-190-28:https  CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51372     37.157.3.29:https      CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51377     server-13-225-38-35:http  ESTABLISHED     14296
 [EpicGamesLauncher.exe]
  TCP    192.168.0.31:51380     a23-57-82-102:https    CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51382     178.250.2.150:https    CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51384     a23-62-197-154:https   ESTABLISHED     12924
 [chrome.exe]
  TCP    192.168.0.31:51386     ny:https               CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51388     bidder:https           CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51389     ny:https               CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51391     chi:https              CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51392     chi:https              CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51403     a23-62-202-235:https   CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51405     chi:https              CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51407     chi:https              CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51408     chi:https              CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51410     chi:https              CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51411     chi:https              CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51414     chi:https              CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51415     chi:https              CLOSE_WAIT      12924
 [chrome.exe]
  TCP    192.168.0.31:51417     13.107.21.200:https    ESTABLISHED     10844
 [SearchApp.exe]
  TCP    192.168.0.31:51418     bingforbusiness:https  ESTABLISHED     10844
 [SearchApp.exe]
  TCP    192.168.0.31:51420     52.114.159.32:https    ESTABLISHED     10844
 [SearchApp.exe]
  TCP    192.168.0.31:51421     52.114.159.32:https    ESTABLISHED     10844
 [SearchApp.exe]
  TCP    192.168.0.31:51429     204.79.197.254:https   ESTABLISHED     10844
 [SearchApp.exe]
  TCP    192.168.0.31:51430     13.107.18.254:https    ESTABLISHED     10844
 [SearchApp.exe]
  TCP    192.168.0.31:51431     13.107.3.254:https     ESTABLISHED     10844
 [SearchApp.exe]
  TCP    192.168.0.31:51432     ec2-34-231-86-172:https  ESTABLISHED     14296
 [EpicGamesLauncher.exe]
  TCP    192.168.0.31:51433     a2-21-35-136:https     ESTABLISHED     7580
 [NVIDIA Share.exe]
  TCP    192.168.0.31:51434     a2-21-35-136:https     ESTABLISHED     7580
 [NVIDIA Share.exe]
  TCP    192.168.0.31:51442     69.94.77.202:http      TIME_WAIT       0
  TCP    192.168.0.31:51443     69.94.77.206:http      TIME_WAIT       0
  TCP    [::]:135               DESKTOP-DG9JKP6:0      LISTENING       1232
  RpcEptMapper
 [svchost.exe]
  TCP    [::]:445               DESKTOP-DG9JKP6:0      LISTENING       4
 Impossible d'obtenir les informations de propri‚taire
  TCP    [::]:7680              DESKTOP-DG9JKP6:0      LISTENING       14856
 Impossible d'obtenir les informations de propri‚taire
  TCP    [::]:49664             DESKTOP-DG9JKP6:0      LISTENING       756
 [lsass.exe]
  TCP    [::]:49665             DESKTOP-DG9JKP6:0      LISTENING       924
 Impossible d'obtenir les informations de propri‚taire
  TCP    [::]:49666             DESKTOP-DG9JKP6:0      LISTENING       1736
  EventLog
 [svchost.exe]
  TCP    [::]:49667             DESKTOP-DG9JKP6:0      LISTENING       2396
  Schedule
 [svchost.exe]
  TCP    [::]:49668             DESKTOP-DG9JKP6:0      LISTENING       2852
  SessionEnv
 [svchost.exe]
  TCP    [::]:49669             DESKTOP-DG9JKP6:0      LISTENING       3456
 [spoolsv.exe]
  TCP    [::]:49671             DESKTOP-DG9JKP6:0      LISTENING       1012
 Impossible d'obtenir les informations de propri‚taire
  TCP    [::1]:49670            DESKTOP-DG9JKP6:0      LISTENING       5636
 [jhi_service.exe]
  UDP    0.0.0.0:5050           *:*                                    9504
  CDPSvc
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*                                    2224
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*                                    12924
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*                                    3692
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:5355           *:*                                    3692
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:27036          *:*                                    13680
 [steam.exe]
  UDP    0.0.0.0:50523          *:*                                    13580
 [Discord.exe]
  UDP    0.0.0.0:54915          *:*                                    11280
 [lghub_agent.exe]
  UDP    0.0.0.0:63348          *:*                                    13680
 [steam.exe]
  UDP    0.0.0.0:65500          *:*                                    4700
 [nvcontainer.exe]
  UDP    127.0.0.1:1900         *:*                                    16512
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:10010        *:*                                    9192
 [NVIDIA Web Helper.exe]
  UDP    127.0.0.1:10011        *:*                                    8288
 [nvcontainer.exe]
  UDP    127.0.0.1:49664        *:*                                    5364
  iphlpsvc
 [svchost.exe]
  UDP    127.0.0.1:52356        *:*                                    8236
 [nvcontainer.exe]
  UDP    127.0.0.1:55923        *:*                                    16512
  SSDPSRV
 [svchost.exe]
  UDP    192.168.0.31:137       *:*                                    4
 Impossible d'obtenir les informations de propri‚taire
  UDP    192.168.0.31:138       *:*                                    4
 Impossible d'obtenir les informations de propri‚taire
  UDP    192.168.0.31:1900      *:*                                    16512
  SSDPSRV
 [svchost.exe]
  UDP    192.168.0.31:2177      *:*                                    10064
  QWAVE
 [svchost.exe]
  UDP    192.168.0.31:5353      *:*                                    4700
 [nvcontainer.exe]
  UDP    192.168.0.31:55922     *:*                                    16512
  SSDPSRV
 [svchost.exe]
  UDP    [::]:54915             *:*                                    11280
 [lghub_agent.exe]
  UDP    [::]:65501             *:*                                    4700
 [nvcontainer.exe]
  UDP    [::1]:1900             *:*                                    16512
  SSDPSRV
 [svchost.exe]
  UDP    [::1]:5353             *:*                                    4700
 [nvcontainer.exe]
  UDP    [::1]:55921            *:*                                    16512
  SSDPSRV
 [svchost.exe]
```
- svchost.exe : processus générique (generic host process ) pour les services exécutés à partir de bibliothèques dynamiques
- steam.exe : plateforme de distribution de contenu en ligne, de gestion des droits et de communication
- lsass.exe : exécutable qui est nécessaire pour le bon fonctionnement de Windows
- spoolsv.exe : processus générique de Windows NT/2000/XP servant à mettre en mémoire (file d'attente) les travaux d'impression
- parsecd.exe : partie d'un produit appelé Parsec dont le développeur est Parsec
- Discord.exe : logiciel propriétaire gratuit de VoIP conçu initialement pour les communautés de joueurs
- lghub.exe : fichier exécutable qui appartient au processus LGHUB Agent fourni avec le logiciel LGHUB Agent développé par Logitech
- lghub_updater.exe : fichier exécutable qui appartient au processus LGHUB Agent fourni avec le logiciel LGHUB Agent développé par Logitech
- nvcontainer.exe : Service conteneur pour les fonctionnalités racine NVIDIA.
- NVIDIA Web Helper.exe : processus qui écoute ou envoie des données sur les ports ouverts du LAN ou sur Internet. NVIDIA Web Helper.exe est capable de gérer des applications et contrôler d'autres programmes
- NVIDIA Share.exe : processus capable d’enregistrer des entrées au clavier et à la souris, de manipuler d’autres programmes et de contrôler des applications
- lghub.exe : Logitech G HUB est le nouvel assistant conçu pour vous aider à exploiter pleinement le potentiel de votre équipement
- EpicGamesLauncher.exe : logiciel permettant d'acheter puis de gérer les différents jeux vidéo obtenus via l'Epic Games Store
- chrome.exe : fichier exécutable qui exécute le navigateur Web Google Chrome, un logiciel gratuit qui affiche des pages Web
- Video.UI.exe : composant logiciel de Xbox Live Entertainment Platform de Microsoft Corporation
- SearchApp.exe : outil de recherche interne exécuté par Cortana
- jhi_service.exe : composant logiciel de Intel® Management Engine Components de Intel Corporation

##

## Scripting : 

```
                                        Script 1 :
```
```
#auteur du script : Matthias Flament
#écrit le : 19/10/2020
#ce script affiche : -un résumé de l'OS
#                    -calcul et affiche le temps de réponse moyen vers 8.8.8.8

echo "Script 1"
echo " "
echo "Detail de L'os:"
echo "----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

#Affiche le nom de l'ordi
echo "Nom de l'ordi :"
[system.environment]::MachineName #est la commande qui affiche seulement le nom de la machine
echo " "

#Affiche L'IP Principale
echo "IP Principale :"
Write-Host "$(((ipconfig | findstr [0-9]..)[6]).Split()[-1])" ;#est la commande qui affiche seulement l'adresse IPv4 de la machine
echo " "

#Affiche OS et version de l'OS
echo "OS et version de l'OS :"
(Get-WmiObject -class Win32_OperatingSystem).Caption #Affiche quel OS est installé
((systeminfo | findstr [0-9]..)[2]).Split()[27] #Affiche la version de l'OS
echo " "

#Afficher date et heure d'allumage
echo "date et heure d'allumage :"
Write-Host "$(((net statistics workstation | findstr [0-9]..)[1]).Split()[2]) | $(((net statistics workstation | findstr [0-9]..)[1]).Split()[3])" #Affiche la date et l'heure de l'allumage
echo " "

#Affiche si l'OS est a jour
echo "determine si l'OS est a jour :"
$updateObject = New-Object -ComObject Microsoft.Update.Session
$updateObject.ClientApplicationID = "Serverfault Example Script"
$updateSearcher = $updateObject.CreateUpdateSearcher()
$searchResults = $updateSearcher.Search("IsInstalled=0")
if ($searchResults = "0") {
  "Windows est a jour"
}else{
  "Windows n'est pas a jour"
}
echo " "

#Affiche la RAM dispo et RAM utilisée
$a = $(Get-CimInstance Win32_PhysicalMemory | Measure-Object -Property capacity -Sum).sum /1MB #initialise a sur la capacité de la ram
$b = $(Get-Ciminstance Win32_OperatingSystem).FreePhysicalMemory*1KB/1MB #Initialise b sur la ram libre
$c = $a-$b #Initialise c avec la soustraction de la capacité totale de la ram et l'espace libre, c = a l'espace utilisé
Write-Host "RAM: "
Write-Host " Used: "-nonewline; Write-Host  "$c Mo" #Affiche c
Write-Host " Free: "-nonewline; Write-Host "$b Mo" #Affiche b
echo " "

#Affiche l'espace disque dispo et utilisé
$l = $(Get-WmiObject -Class Win32_logicaldisk).Size*1KB/1TB #Initialise l sur la capacité du disque dur
$e = $(Get-WmiObject -Class Win32_logicaldisk).FreeSpace*1KB/1TB #Initialise e sur l'espace du disque dur libre
$o = $l-$e #Initialise o avec la soustraction de la capacité totale de la ram et l'espace libre, o = l'espace utilisé
Write-Host "Disk: "
Write-Host " Used: "-nonewline; Write-Host  "$o Go" #Affiche o
Write-Host " Free: "-nonewline; Write-Host "$e Go" #Affiche e
echo " "

Write-Host "User list: "-nonewline; Write-Host "$((Get-WmiObject Win32_UserAccount).Name)" #Donne la liste de tous les noms de tous les utilisateurs de la machine
echo " "

Write-Host "- 8.8.8.8 average ping time :"-nonewline; Write-Host "$(((ping 8.8.8.8 | findstr [0-9]..)[7]).Split()[-1])" #Donne le temps de réponse moyen vers 8.8.8.8
```

```
                                        Script 2:
```
```
$param1 = $args[0] 
[int] $temps = $args[1]
if ($param1 = "lock") {
    #Verouille l'ordinateur après "temps" secondes
    Start-Sleep -Seconds $temps | rundll32.exe user32.dll, LockWorkStation
}
elseif ($param1 = "shutdown") {
    #Eteint l'ordinateur après "temps" secondes
    Start-Sleep -Seconds $temps | Stop-Computer
}
```

##

## Gestion de Softs :
### **Utilité des gestionnaires de paquets :**
>**par rapport au téléchargement en direct sur internet**

Tous les logiciels (les paquets) sont centralisés dans un seul et même serveur (le dépôt) : plus besoin de parcourir le Web pour trouver les fichiers d’installation de tel ou tel logiciel, tous se trouvent au même endroit ! On va enfin pouvoir installer et mettre à jour plusieurs logiciels avec une seule commande.

>**l'identité des gens impliqués dans un téléchargement**

?

>**La sécurité globale**

Tous les logiciels sont dépourvus de spywares et malwares en tout genre : toutes les cases qui installaient des logiciels non désirés n’existent plus.

## 

### **Utilisation Chocolatey :**

>**lister tous les paquets déjà installés**
```
choco list -l
Liste tous les paquets installés
```
>>**Résultat**
```
Chocolatey v0.10.15
chocolatey 0.10.15
1 packages installed.
```
##

>**déterminer la provenance des paquets**
```
choco info
affiche les informations du paquet
```
>>**Résultat**
```

```

##

## Machine Virtuelle :

### **Configuration de VirtualBox :**

Vu en cours

## 

### **Installation de la VM :**

Vu en cours

## 

### **Installation de l'OS :**

Vu en cours

## 

### **Configuration post-install :**

Vu en cours

## 

### **Partage de fichiers :**
###### ***Avant nouvelle partie du TP***
>**prouver qu'un processus/service est dédié à ce partage**
```
sc.exe qc lanmanworkstation
Affiche quel service est utilisé pour le partage de fichier
```
>>**résultat : **
```
SERVICE_NAME: lanmanworkstation
        TYPE               : 20  WIN32_SHARE_PROCESS
        START_TYPE         : 2   AUTO_START
        ERROR_CONTROL      : 1   NORMAL
        BINARY_PATH_NAME   : C:\WINDOWS\System32\svchost.exe -k NetworkService -p
        LOAD_ORDER_GROUP   : NetworkProvider
        TAG                : 0
        DISPLAY_NAME       : Station de travail
        DEPENDENCIES       : Bowser
                           : MRxSmb20
                           : NSI
        SERVICE_START_NAME : NT AUTHORITY\NetworkService
```

>**prouver qu'un port réseau permet d'y accéder**
```
Get-WindowsOptionalFeature –Online –FeatureName SMB1Protocol
Affiche quel port réseau est utilisé pour y accéder
```
>>**résultat : **
```
FeatureName      : SMB1Protocol
DisplayName      : Support de partage de fichiers SMB 1.0/CIFS
Description      : Support du protocole de partage de fichiers SMB 1.0/CIFS et du protocole Explorateur d’ordinateurs.
RestartRequired  : Possible
State            : Disabled
CustomProperties :
                   ServerComponent\Description : Support du protocole de partage de fichiers SMB 1.0/CIFS et du
                   protocole Explorateur d’ordinateurs.
                   ServerComponent\DisplayName : Support de partage de fichiers SMB 1.0/CIFS
                   ServerComponent\Id : 487
                   ServerComponent\Type : Feature
                   ServerComponent\UniqueName : FS-SMB1
                   ServerComponent\Deploys\Update\Name : SMB1Protocol
```
## 

### **prouver qu'un client peut y accéder:**
```
Get-SmbServerConfiguration | Select AuditSmb1Access
Affiche qui a accès aux fichiers partagés
```
Si le Résultat est : 
```
AuditSmb1Access
---------------
          False
```
Alors personne n'y a accès, il faut donc rentrer la commande : 
```
Set-SmbServerConfiguration -AuditSmb1Access $true
```
Qui donnera comme résultat : 
```
AuditSmb1Access
---------------
           True
```

###### ***Après nouvelle partie du TP***

>**Dans la machine virtuelle**
```
yum install -y cifs-utils
permet d'installer le package cifs-utils
```
>**Résultat**
```
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: mirrors.ircam.fr
 * extras: mirror.ircam.fr
 * updates: mirrors.ircam.fr
base
extras
updates
(1/3): extras/7/x86_64/primary_db
(2/3): updates/7/x86_64/primary_db
(3/3): base/7/x86_64/primary_db
Package cifs-utils-6.2-10.e17.x86_64 already installed and latest version
Nothing to do
```

>**Dans la machine virtuelle**
```
mkdir /opt/partage
cd opt
ls

Permet de créer un dossier partage puis vérification qu'il a bien été crée
```
>**Résultat**
```
[root@TP1 opt]# ls
partage
```
>**Dans la machine virtuelle**
```
mount -t cifs -o username=Bmattf,password=********* //192.168.120.1/testpartage /opt/partage
cd partage
ls
Monte le partage dans la VM puis vérification
```
>**Résultat**
```
[root@TP1 opt]# mount -t cifs -o username=Bmattf,password=********* //192.168.120.1/testpartage /opt/partage
[root@TP1 opt]# cd partage/
[root@TP1 partage]# ls
partage.txt
```